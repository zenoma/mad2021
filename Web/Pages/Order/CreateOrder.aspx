﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ECommerce.Master" AutoEventWireup="true" CodeBehind="CreateOrder.aspx.cs" Inherits="Es.Udc.DotNet.PracticaMaD.Web.Pages.Order.CreateOrder" %>

<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder_BodyContent" runat="server">
        <h1>
            <asp:Localize ID="titlePage" meta:resourcekey="titlePage" runat="server"></asp:Localize>
        </h1>
        <div class="containerErrors">
            <asp:Label ID="lblIdentifierError" runat="server" CssClass="errorMessage" meta:resourcekey="lblIdentifierError" />
        </div>
        <div class="field">
            <span class="label">
                <asp:Localize ID="lclName" runat="server" meta:resourcekey="lclName" /></span><span class="entry">
                    <asp:TextBox ID="txtName" runat="server" Columns="16"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvName" runat="server"
                        ControlToValidate="txtName" Font-Bold="true" ForeColor="Red" Display="Dynamic" Text="<%$ Resources:Common, mandatoryField %>" /></span>
        </div>
        <div class="field">
            <span class="label">
                <asp:Localize ID="lclPostalAddress" runat="server" meta:resourcekey="lclPostalAddress" /></span><span class="entry">
                    <asp:TextBox ID="txtPostalAddress" runat="server" Columns="16"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvPostalAddress" runat="server"
                        ControlToValidate="txtPostalAddress" Font-Bold="true" ForeColor="Red" Display="Dynamic" Text="<%$ Resources:Common, mandatoryField %>" /></span>
        </div>
        <div class="field">
            <span class="label">
                <asp:Localize ID="lclCreditCard" runat="server" meta:resourcekey="lclCreditCard" />
            </span>
            <span class="entry">
                <asp:DropDownList ID="comboCreditCard" runat="server">
                </asp:DropDownList>
                <asp:HyperLink ID="lnkAddCreditCard" runat="server"
                    NavigateUrl="~/Pages/User/AddCreditCard.aspx?ReturnUrl=~/Pages/Order/CreateOrder.aspx"
                    meta:resourcekey="lnkAddCreditCard" />
            </span>
        </div>
        <div class="button">
            <asp:Button ID="btnCreateOrder" runat="server" OnClick="BtnCreateOrder" meta:resourcekey="btnCreateOrder" />
        </div>
</asp:Content>
