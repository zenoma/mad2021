﻿using System;

namespace Es.Udc.DotNet.PracticaMaD.Model.Model1Daos.MusicDao
{
    public interface IMusicDao : ModelUtil.Dao.IGenericDao<Music, Int64>
    {
    }
}
