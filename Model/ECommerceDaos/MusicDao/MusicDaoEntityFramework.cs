﻿using Es.Udc.DotNet.ModelUtil.Dao;
using System;

namespace Es.Udc.DotNet.PracticaMaD.Model.Model1Daos.MusicDao
{
    public class MusicDaoEntityFramework : GenericDaoEntityFramework<Music, Int64>, IMusicDao
    {
    }
}
