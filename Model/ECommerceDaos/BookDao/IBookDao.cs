﻿using System;

namespace Es.Udc.DotNet.PracticaMaD.Model.Model1Daos.BookDao
{
    public interface IBookDao : ModelUtil.Dao.IGenericDao<Book, Int64>
    {
    }
}
