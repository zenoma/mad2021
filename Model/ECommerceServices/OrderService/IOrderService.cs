﻿using Es.Udc.DotNet.ModelUtil.Transactions;
using Es.Udc.DotNet.PracticaMaD.Model.ECommerceServices.CartService;
using Es.Udc.DotNet.PracticaMaD.Model.Model1Daos.CreditCardDao;
using Es.Udc.DotNet.PracticaMaD.Model.Model1Daos.OrderDao;
using Es.Udc.DotNet.PracticaMaD.Model.Model1Daos.OrderItemDao;
using Es.Udc.DotNet.PracticaMaD.Model.Model1Daos.ProductDao;
using Es.Udc.DotNet.PracticaMaD.Model.Model1Daos.UserDao;
using Ninject;

namespace Es.Udc.DotNet.PracticaMaD.Model.ECommerceServices.OrderService
{
    public interface IOrderService
    {

        [Inject]
        IOrderDao orderDao { set; }

        [Inject]
        IOrderItemDao orderItemDao { set; }

        [Inject]
        IUserDao userDao { set; }

        [Inject]
        IProductDao productDao { set; }

        [Inject]
        ICreditCardDao creditCardDao { set; }

        [Transactional]
        OrderDto CreateOrder(string login, CartDto cart, long creditCardNumber, string address, string orderAlias);

        [Transactional]
        OrderBlock FindByUserLogin(string login, int page, int count);

        [Transactional]
        OrderDto FindByOrderId(long orderId);

    }
}
